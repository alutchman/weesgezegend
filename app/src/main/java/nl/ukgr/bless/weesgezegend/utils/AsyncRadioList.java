package nl.ukgr.bless.weesgezegend.utils;

import android.os.AsyncTask;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import nl.ukgr.bless.weesgezegend.values.RadioItem;

import static nl.ukgr.bless.weesgezegend.utils.XmlNodeHelper.getTagNodeValue;

/**
 * Created by amrit on 11/2/17.
 */

public abstract class AsyncRadioList extends AsyncTask<URL, Void, List<RadioItem>> {

    private static List<RadioItem> prevList = null;
    private static RadioItem errorItem = new RadioItem("Radio Positief", "http://radiopositief.live-streams.nl:80/live");
    public abstract void afterLoading(List<RadioItem> radioList);

    @Override
    protected List<RadioItem> doInBackground(URL... urls) {
        //== Avoid reloading data =====
        if (prevList != null) {
            return prevList;
        }
        List<RadioItem> radioInfoList = new ArrayList<>();
        try {
            URL url = urls[0];
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;
            db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(url.openStream()));
            fillStations(doc, radioInfoList);
            prevList = radioInfoList;
        } catch (Exception e1) {
            radioInfoList.add(errorItem);
        }
        return radioInfoList;
    }

    private void fillStations(Document doc, List<RadioItem> radioInfoList){
        doc.getDocumentElement().normalize();
        NodeList nodeList = doc.getElementsByTagName("station");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            Element fstElmnt = (Element) node;
            String name = getTagNodeValue(fstElmnt, "title");
            String urlRadio = getTagNodeValue(fstElmnt, "url");
            RadioItem info = new RadioItem(name, urlRadio);
            radioInfoList.add(info);
        }
    }

    @Override
    protected void onPostExecute(List<RadioItem> radioList) {
        afterLoading(radioList);
    }
}
