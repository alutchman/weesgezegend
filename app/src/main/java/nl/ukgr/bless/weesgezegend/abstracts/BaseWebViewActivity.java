package nl.ukgr.bless.weesgezegend.abstracts;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;


import java.util.List;

import nl.ukgr.bless.weesgezegend.R;
import nl.ukgr.bless.weesgezegend.adapters.WebViewPagerAdapter;
import nl.ukgr.bless.weesgezegend.utils.AsyncStartViews;
import nl.ukgr.bless.weesgezegend.values.StartViewItem;


/**
 * Created by amrit on 11/10/17.
 */

public abstract class BaseWebViewActivity extends CoreWebActivity {
    protected WebViewPagerAdapter mSectionsPagerAdapter;
    protected ViewPager mViewPager;

    public abstract String getUrl();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_pull);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mViewPager = (ViewPager) findViewById(R.id.container);
        new BaseWebViewActivity.AsyncStartViewsImpl().
                execute(getUrl());
    }

    private class AsyncStartViewsImpl extends AsyncStartViews {
        @Override
        public void afterLoading(List<StartViewItem> viewList) {
            mSectionsPagerAdapter = new WebViewPagerAdapter(getSupportFragmentManager(),viewList);
            mViewPager.setAdapter(mSectionsPagerAdapter);
        }
    }




}
