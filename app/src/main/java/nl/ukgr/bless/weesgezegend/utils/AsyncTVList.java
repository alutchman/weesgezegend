package nl.ukgr.bless.weesgezegend.utils;

import android.os.AsyncTask;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import nl.ukgr.bless.weesgezegend.values.VideoItem;

import static nl.ukgr.bless.weesgezegend.utils.XmlNodeHelper.getTagNodeValue;

/**
 * Created by amrit on 11/7/17.
 */

public abstract class AsyncTVList extends AsyncTask<URL, Void, List<VideoItem>> {
    private static List<VideoItem> prevList = null;
    private static VideoItem errorItem = new VideoItem("Portugees Brasil", "http://iuhls-lh.akamaihd-staging.net/i/utvhls1_1@336317/master.m3u8");
    public abstract void afterLoading(List<VideoItem> radioList);

    @Override
    protected List<VideoItem> doInBackground(URL... urls) {
        //== Avoid reloading data =====
        if (prevList != null) {
            return prevList;
        }
        List<VideoItem> tvInfoList = new ArrayList<>();
        try {
            URL url = urls[0];
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;
            db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(url.openStream()));
            fillStations(doc, tvInfoList);
            prevList = tvInfoList;
        } catch (Exception e1) {
            tvInfoList.add(errorItem);
        }
        return tvInfoList;
    }

    private void fillStations(Document doc, List<VideoItem> tvInfoList){
        doc.getDocumentElement().normalize();
        NodeList nodeList = doc.getElementsByTagName("station");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            Element fstElmnt = (Element) node;
            String name = getTagNodeValue(fstElmnt, "title");
            String urlRadio = getTagNodeValue(fstElmnt, "url");
            VideoItem info = new VideoItem(name, urlRadio);
            tvInfoList.add(info);
        }
    }

    @Override
    protected void onPostExecute(List<VideoItem> tvInfoList) {
        afterLoading(tvInfoList);
    }
}
