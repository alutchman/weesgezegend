package nl.ukgr.bless.weesgezegend.values;

/**
 * Created by amrit on 11/2/17.
 */

public class VideoItem {
    private static int counter = 1;
    private int id;
    private String name;
    private String url;


    public VideoItem(String name, String url){
        this.name = name;
        this.url  = url;
        this.id = counter++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return name;
    }
}
