package nl.ukgr.bless.weesgezegend.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import nl.ukgr.bless.weesgezegend.fragments.WebViewFragment;
import nl.ukgr.bless.weesgezegend.values.StartViewItem;

/**
 * Created by amrit on 10/21/17.
 */

public class WebViewPagerAdapter extends FragmentPagerAdapter {
    private List<StartViewItem> viewList;

    public WebViewPagerAdapter(FragmentManager fm, List<StartViewItem> viewList) {
        super(fm);
        this.viewList = viewList;
    }

    @Override
    public Fragment getItem(int position) {

        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        StartViewItem current =  viewList.get(position);
        return WebViewFragment.newInstance(current);
    }

    @Override
    public int getCount() {
        return viewList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position < viewList.size()) {
            StartViewItem current =  viewList.get(position);
            return current.getName();
        }
        return "....";
    }
}
