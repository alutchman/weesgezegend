package nl.ukgr.bless.weesgezegend.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import nl.ukgr.bless.weesgezegend.R;
import nl.ukgr.bless.weesgezegend.values.StartViewItem;

/**
 * Created by amrit on 10/21/17.
 */

public class WebViewFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "zegenPagina";
    private static final String ARG_SECTION_TITLE = "zegenTitel";
    private static final String ARG_FIRST_VIEW = "isFirstView";
    private static final String ARG_LAST_VIEW = "isLastView";
    private static final String ARG_WEB_URL = "WEB_URL";


    public static Fragment newInstance(StartViewItem startViewItem) {
        WebViewFragment fragment = new WebViewFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, startViewItem.getId());
        args.putString(ARG_SECTION_TITLE, startViewItem.getName());
        args.putBoolean(ARG_FIRST_VIEW, startViewItem.isFirstView());
        args.putBoolean(ARG_LAST_VIEW, startViewItem.isLastView());
        args.putString(ARG_WEB_URL, startViewItem.getUrl());
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_web_pull, container, false);
        TextView textView = rootView.findViewById(R.id.section_label);

        String title = getArguments().getString(ARG_SECTION_TITLE);
        textView.setText(title);

        boolean isFirstView = getArguments().getBoolean(ARG_FIRST_VIEW);
        boolean isLastView = getArguments().getBoolean(ARG_LAST_VIEW);

        String url =  getArguments().getString(ARG_WEB_URL);

        WebView myWebView = rootView.findViewById(R.id.wvWelkom);
        myWebView.setWebChromeClient(new WebChromeClient());
        myWebView.setWebViewClient(new WebViewClient());

        TextView chapterLeft  = rootView.findViewById(R.id.imageLeft);
        TextView chapterRight = rootView.findViewById(R.id.imageRight);
        chapterLeft.setMaxWidth(20);
        chapterRight.setMaxWidth(20);

        chapterLeft.setVisibility(!isFirstView ? View.VISIBLE : View.INVISIBLE );
        chapterRight.setVisibility(!isLastView ? View.VISIBLE : View.INVISIBLE );

        myWebView.loadUrl(url);

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setBuiltInZoomControls(true);
        return rootView;
    }


}
