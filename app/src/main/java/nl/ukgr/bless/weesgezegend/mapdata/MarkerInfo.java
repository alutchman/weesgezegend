package nl.ukgr.bless.weesgezegend.mapdata;

/**
 * Created by amrit on 10/14/17.
 */

public class MarkerInfo {
    double lat;
    double lon;
    String name;
    String snippet;
    int id ;
    int type;

    public MarkerInfo(int id, int type, String name, double la, double lo, String descr) {
        this.id = id;
        this.type = type;
        this.name = name;
        lat = la;
        lon = lo;
        snippet = descr;

    }
}
