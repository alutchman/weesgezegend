package nl.ukgr.bless.weesgezegend.abstracts;

import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import nl.ukgr.bless.weesgezegend.R;
import nl.ukgr.bless.weesgezegend.utils.SwitchToIntent;

/**
 * Created by amrit on 11/13/17.
 */

public abstract class CoreWebActivity extends AppCompatActivity {
    public abstract void editMenu(Menu menu);

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_global, menu);
        editMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = SwitchToIntent.handleMenuItem(this, item);
        if (handled) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
