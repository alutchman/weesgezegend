package nl.ukgr.bless.weesgezegend.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import nl.ukgr.bless.weesgezegend.R;
import nl.ukgr.bless.weesgezegend.abstracts.CoreWebActivity;
import nl.ukgr.bless.weesgezegend.utils.SwitchToIntent;

public class WebRadioActivity extends CoreWebActivity {
    private static boolean isDevelopping = false;

    private final static String URL_UKGR = "http://www.ukgr.nl/app/media/radioWeesGezegend.php";
    private final static String URL_LOCAL = "http://192.168.1.4/ukgrappdata/media/radioWeesGezegend.php";
    private final static String radioWEB = isDevelopping ? URL_LOCAL : URL_UKGR;

    private WebView myWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_radio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        myWebView = (WebView) findViewById(R.id.RadioWebView);
        myWebView.setWebChromeClient(new WebChromeClient());
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.loadUrl(radioWEB);

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setBuiltInZoomControls(true);

    }

    @Override
    public void editMenu(Menu menu) {
        SwitchToIntent.disableMenuItem(menu, 2);
    }


    @Override
    protected void onDestroy() {
        if (myWebView != null) {
            myWebView.loadUrl("http://www.google.nl");
            myWebView = null;
        }
        super.onDestroy();
    }


    @Override
    public void onPause() {
        super.onPause();
        myWebView.onPause();
    }

    @Override
    public void onBackPressed() {
        myWebView.removeAllViews();
        super.onBackPressed();
    }
}
