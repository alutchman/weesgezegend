package nl.ukgr.bless.weesgezegend.utils;

import android.os.AsyncTask;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import nl.ukgr.bless.weesgezegend.values.StartViewItem;

import static nl.ukgr.bless.weesgezegend.utils.XmlNodeHelper.getTagNodeValue;

/**
 * Created by amrit on 11/7/17.
 */
public abstract class AsyncStartViews extends AsyncTask<String, Void, List<StartViewItem>> {
    private final static String ITEM_TAG = "view";
    private final static String NAME_TAG = "title";
    private final static String URL_TAG = "url";

    private static Map<String,List<StartViewItem>> prevItems = new HashMap<>();


    private static StartViewItem errorItem = new StartViewItem("Agenda",
            "http://www.ukgr.nl/app/agenda.php", true, true);

    public abstract void afterLoading(List<StartViewItem> viewList);

    @Override
    protected List<StartViewItem> doInBackground(String... urls) {
        //== Avoid reloading data =====
        if (prevItems.containsKey(urls[0])) {
            return prevItems.get(urls[0]);
        }
        List<StartViewItem> viewList = new ArrayList<>();
        try {
            URL url = new URL(urls[0]);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;
            db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(url.openStream()));
            fillViews(doc, viewList);
            prevItems.put(urls[0], viewList);
        } catch (Exception e1) {
            viewList.add(errorItem);
        }
        return viewList;
    }

    private void fillViews(Document doc, List<StartViewItem> viewList){
        doc.getDocumentElement().normalize();
        NodeList nodeList = doc.getElementsByTagName(ITEM_TAG);
        int lastIndex = nodeList.getLength()-1;
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            Element fstElmnt = (Element) node;
            String name = getTagNodeValue(fstElmnt, NAME_TAG);
            String urlRadio = getTagNodeValue(fstElmnt, URL_TAG);
            StartViewItem info = new StartViewItem(name, urlRadio, i== 0, i==lastIndex);
            viewList.add(info);
        }
    }

    @Override
    protected void onPostExecute(List<StartViewItem> viewList) {
        afterLoading(viewList);

    }
}
