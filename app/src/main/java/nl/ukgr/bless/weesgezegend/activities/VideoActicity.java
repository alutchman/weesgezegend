package nl.ukgr.bless.weesgezegend.activities;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.MediaController;
import android.widget.Spinner;
import android.widget.VideoView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import nl.ukgr.bless.weesgezegend.R;
import nl.ukgr.bless.weesgezegend.abstracts.CoreWebActivity;
import nl.ukgr.bless.weesgezegend.utils.AsyncTVList;
import nl.ukgr.bless.weesgezegend.utils.SwitchToIntent;
import nl.ukgr.bless.weesgezegend.values.VideoItem;

public class VideoActicity extends CoreWebActivity implements AdapterView.OnItemSelectedListener {
    private static boolean isDevelopping = false;

    private final static String URL_UKGR = "http://www.ukgr.nl/app/media/tv.xml";
    private final static String URL_LOCAL = "http://192.168.1.4/ukgrappdata/media/tv.xml";
    private final static String LIST_OF_TV = isDevelopping ? URL_LOCAL : URL_UKGR;

    private VideoView videoView;
    private MediaController mediaControls;
    private Spinner stations;
    private boolean firstRun = true;

    /*
        Spaans  USA:
        http://iuhls4-lh.akamaihd.net/i/utvesphls1_1@348620/master.m3u8


        Venezuela SPAANS
        http://lsdiurdhls-lh.akamaihd.net/i/IURDTVHLS_1@197885/master.m3u8

        Brasil Portugees :
        http://iuhls-lh.akamaihd-staging.net/i/utvhls1_1@336317/master.m3u8
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iurd_video);

        stations = (Spinner) findViewById(R.id.videoList);
        videoView = (VideoView) findViewById(R.id.iurdVideoID);

        //set the media controller buttons
        if (mediaControls == null) {
            mediaControls = new MediaController(VideoActicity.this);
        }
        //videoView.setOnPreparedListener(this);
        stations.setOnItemSelectedListener(this);
        try {
            new ImplAsyncTVList().
                    execute(new URL( LIST_OF_TV));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void editMenu(Menu menu) {
        SwitchToIntent.disableMenuItem(menu, 3);
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //we use onSaveInstanceState in order to store the video playback position for orientation change
        savedInstanceState.putInt("Position", videoView.getCurrentPosition());
        videoView.pause();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position >= 0 && position < parent.getCount()) {
            VideoItem selected = (VideoItem) parent.getSelectedItem();
            if (selected != null && videoView != null) {
                boolean wasPlaying = (videoView != null && videoView.isPlaying());
                if (firstRun) {
                    firstRun = false;
                    wasPlaying = true;
                }
                if (wasPlaying) {
                    videoView.stopPlayback();
                }
                try {
                    videoView.setVideoURI(Uri.parse(selected.getUrl()));
                    videoView.setMediaController(mediaControls);
                    mediaControls.setAnchorView(videoView);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (wasPlaying) {
                        videoView.start();
                    }
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class ImplAsyncTVList extends AsyncTVList {
        @Override
        public void afterLoading(List<VideoItem> tvList) {
            ArrayAdapter<VideoItem> dataAdapter = new ArrayAdapter<>( VideoActicity.this.getApplicationContext(),
                    android.R.layout.simple_spinner_item, tvList);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            stations.setAdapter(dataAdapter);
            stations.setBackgroundColor(Color.WHITE);
            stations.setFocusable(true);
            stations.setSelection(0);
        }
    }


}
