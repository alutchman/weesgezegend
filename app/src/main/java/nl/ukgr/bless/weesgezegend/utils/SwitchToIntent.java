package nl.ukgr.bless.weesgezegend.utils;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import nl.ukgr.bless.weesgezegend.R;
import nl.ukgr.bless.weesgezegend.activities.MapsOct17Activity;
import nl.ukgr.bless.weesgezegend.activities.StoriesActivity;
import nl.ukgr.bless.weesgezegend.activities.VideoActicity;
import nl.ukgr.bless.weesgezegend.activities.WebPullActivity;
import nl.ukgr.bless.weesgezegend.activities.WebRadioActivity;

/**
 * Created by amrit on 11/7/17.
 */

public class SwitchToIntent {
    private static void stopLastIntent(Activity currentActivity) {
        if (currentActivity != null && currentActivity.getIntent() != null) {
            currentActivity.stopService(currentActivity.getIntent());
        }
    }

    public static void location(Activity currentActivity) {
        Intent intent = new Intent(currentActivity, MapsOct17Activity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        stopLastIntent(currentActivity);
        currentActivity.startActivityIfNeeded(intent, 0);

    }

    public static void radio(Activity currentActivity) {
        Intent intent = new Intent(currentActivity, WebRadioActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        stopLastIntent(currentActivity);
        currentActivity.startActivityIfNeeded(intent, 0);
    }

    public static void start(Activity currentActivity) {
        Intent intent = new Intent(currentActivity, WebPullActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        stopLastIntent(currentActivity);
        currentActivity.startActivityIfNeeded(intent, 0);
    }

    public static void video(Activity currentActivity) {
        Intent intent = new Intent(currentActivity, VideoActicity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        stopLastIntent(currentActivity);
        currentActivity.startActivityIfNeeded(intent, 0);
    }


    public static void stories(Activity currentActivity) {
        Intent intent = new Intent(currentActivity, StoriesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        stopLastIntent(currentActivity);
        currentActivity.startActivityIfNeeded(intent, 0);
    }


    public static boolean handleMenuItem(Activity currentActivity, MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_stories) {
            SwitchToIntent.stories(currentActivity);
            return true;
        }


        if (id == R.id.action_locaties) {
            SwitchToIntent.location(currentActivity);
            return true;
        }

        if (id == R.id.action_radio) {
            SwitchToIntent.radio(currentActivity);
            return true;
        }

        if (id == R.id.action_video) {
            SwitchToIntent.video(currentActivity);
            return true;
        }

        if (id == R.id.action_informatie) {
            SwitchToIntent.start(currentActivity);
            return true;
        }

        return false;
    }


    public static void disableMenuItem(Menu menu, int disableIndex) {
        try {
            menu.getItem(disableIndex).setEnabled(false);
        } catch (Exception e){

        }
    }

}