package nl.ukgr.bless.weesgezegend.activities;

import android.view.Menu;

import nl.ukgr.bless.weesgezegend.abstracts.BaseWebViewActivity;
import nl.ukgr.bless.weesgezegend.utils.SwitchToIntent;

public class WebPullActivity extends BaseWebViewActivity {
    private static boolean isDevelopping = false;

    private final static String URL_UKGR = "http://www.ukgr.nl/app/startViews.xml";
    private final static String URL_LOCAL = "http://192.168.1.4/ukgrappdata/startViews.xml";


    @Override
    public String getUrl() {
        return isDevelopping ? URL_LOCAL : URL_UKGR;
    }

    @Override
    public void editMenu(Menu menu) {
        SwitchToIntent.disableMenuItem(menu, 0);
    }


}
