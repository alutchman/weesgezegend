package nl.ukgr.bless.weesgezegend.utils;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlNodeHelper {
    public static String getTagNodeValue(Element fstElmnt, String tagName) {
        NodeList gelList = fstElmnt.getElementsByTagName(tagName);
        Element genElement = (Element) gelList.item(0);
        gelList = genElement.getChildNodes();
        return ((Node) gelList.item(0)).getNodeValue();
    }

    public static double getDoubleTagNodeValue(Element fstElmnt, String tagName) {
        String result =  getTagNodeValue(fstElmnt, tagName);

        return Double.parseDouble(result);
    }

    public static int getIntTagNodeValue(Element fstElmnt, String tagName) {
        String result =  getTagNodeValue(fstElmnt, tagName);

        return Integer.parseInt(result);
    }
}
