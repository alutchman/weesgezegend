package nl.ukgr.bless.weesgezegend.mapdata;

import android.os.AsyncTask;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import static nl.ukgr.bless.weesgezegend.utils.XmlNodeHelper.getDoubleTagNodeValue;
import static nl.ukgr.bless.weesgezegend.utils.XmlNodeHelper.getIntTagNodeValue;
import static nl.ukgr.bless.weesgezegend.utils.XmlNodeHelper.getTagNodeValue;

/**
 * Created by amrit on 10/14/17.
 */

public class GetXmlAsync extends AsyncTask<URL, Void, List<MarkerInfo>> {
    private static final String ukgrLocsRemote = "http://www.ukgr.nl/app/locaties.xml";
    private static final MarkerInfo ERROR_DATA_HOOFD = new MarkerInfo(1,1,"Fruitweg 4, Den Haag (Hoofdkerk)", 52.0608876, 4.3088235, "" );
    private static final LatLng AMERSFOORT = new LatLng(52.1561113,5.3878266);

    private GoogleMap mMap;


    public GetXmlAsync(GoogleMap mMap){
        this.mMap = mMap;
    }


    @Override
    protected List<MarkerInfo> doInBackground(URL... params) {
        List<MarkerInfo>  result = readFromExternal();
        return result;
    }


    private List<MarkerInfo> readFromExternal(){
        List<MarkerInfo> markerInfoList = new ArrayList<>();
        Document doc = null;
        URL url = null;
        try {
            url = new URL(ukgrLocsRemote);

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;
            db = dbf.newDocumentBuilder();
            doc = db.parse(new InputSource(url.openStream()));
            fillMarkers(doc, markerInfoList);

        } catch (Exception e1) {
           markerInfoList.add(ERROR_DATA_HOOFD);
        }
        return markerInfoList;
    }


    private void fillMarkers(Document doc, List<MarkerInfo> markerInfoList){

        doc.getDocumentElement().normalize();
        NodeList nodeList = doc.getElementsByTagName("locatie");


        for (int i = 0; i < nodeList.getLength(); i++) {

            Node node = nodeList.item(i);
            Element fstElmnt = (Element) node;

            int id = getIntTagNodeValue(fstElmnt, "id");
            int type = getIntTagNodeValue(fstElmnt, "type");
            String name = getTagNodeValue(fstElmnt, "name");
            double lat = getDoubleTagNodeValue(fstElmnt, "lat");
            double lng = getDoubleTagNodeValue(fstElmnt, "lng");
            String descr = getTagNodeValue(fstElmnt, "description");

            MarkerInfo info = new MarkerInfo(id, type, name,lat,lng, descr);
            markerInfoList.add(info);
        }
    }




    private static float getColorOfIcon(int type){
        switch (type) {
            case 1   : return BitmapDescriptorFactory.HUE_RED;
            case 2   : return BitmapDescriptorFactory.HUE_ORANGE;
            case 3   : return BitmapDescriptorFactory.HUE_YELLOW;
            default  : return BitmapDescriptorFactory.HUE_AZURE;
        }
    }


    @Override
    protected void onPostExecute(List<MarkerInfo> markerList) {
        double minLatitude = Double.MAX_VALUE;
        double maxLatitude = Double.MIN_VALUE;
        double minLongitude = Double.MAX_VALUE;
        double maxLongitude = Double.MIN_VALUE;

        float colorMain = BitmapDescriptorFactory.HUE_CYAN;

        for (MarkerInfo info : markerList) {
            float colorIcon = getColorOfIcon(info.type);
            if (info.id == 1) {
                mMap.addMarker(new MarkerOptions().position(new LatLng(info.lat, info.lon))
                        .title(info.name).snippet(info.snippet)
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(colorMain)));
            } else {
                mMap.addMarker(new MarkerOptions().position(new LatLng(info.lat, info.lon))
                        .title(info.name).snippet(info.snippet)
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(colorIcon)));
            }
            maxLatitude = Math.max(info.lat, maxLatitude);
            minLatitude = Math.min(info.lat, minLatitude);
            maxLongitude = Math.max(info.lon, maxLongitude);
            minLongitude = Math.min(info.lon, minLongitude);
        }

        maxLatitude += 0.15;
        minLatitude -= 0.05;

        LatLng minXy = new LatLng(minLatitude, minLongitude);
        LatLng maxXy = new LatLng(maxLatitude, maxLongitude);

        LatLngBounds bounds = new LatLngBounds.Builder().include(minXy).include(maxXy).build();

        if (markerList.size() >= 3) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds,1));
        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(AMERSFOORT, 7.4f));
        }
    }
}
