package nl.ukgr.bless.weesgezegend.values;

/**
 * Created by amrit on 11/7/17.
 */

public class StartViewItem {
    private static int counter = 1;
    private int id;
    private final String name;
    private final String url;

    private final boolean firstView;
    private final boolean lastView;


    public StartViewItem( String name,  String url, boolean firstView, boolean lastView){
        this.name = name;
        this.url  = url;
        this.id = counter++;
        this.firstView = firstView;
        this.lastView = lastView;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }


    public boolean isFirstView() {
        return firstView;
    }

    public boolean isLastView() {
        return lastView;
    }

    @Override
    public String toString() {
        return name;
    }
}
